# Security Reports examples

This repository can be used to test out features like

- [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html)
- [Dynamic Application Security Testing reports](https://docs.gitlab.com/ee/user/project/merge_requests/dast.html)
- [Dependency Scanning reports](https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html)
- [Container Scanning reports](https://docs.gitlab.com/ee/user/project/merge_requests/container_scanning.html)
- [License management reports](https://docs.gitlab.com/ee/user/project/merge_requests/license_management.html)

## Testing locally

1. Import Repository into GitLab
2. Run CI on `master`
3. Run CI on `feature-branch`
4. Create Merge request from `feature-branch` to `master`  